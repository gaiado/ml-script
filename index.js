const puppeteer = require('puppeteer');
const XLSX = require('xlsx');
const retry = require('async-retry');

async function obtenerEnlaces(url, currentBrowser) {
    const browser = currentBrowser || await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    await page.goto(url);

    const enlaces = await page.evaluate(() => {
        const enlaces = [];
        const elementos = document.querySelectorAll('a.ui-search-link__title-card');
        elementos.forEach(elemento => {
            enlaces.push(elemento.href);
        });
        return enlaces;
    });

    const siguiente = await page.evaluate(() => {
        const elemento = document.querySelector('.andes-pagination__button:not(.andes-pagination__button--disabled) .andes-pagination__link[title="Siguiente"');
        return elemento ? elemento.href : null;
    });


    if (siguiente) {
        return [...(await obtenerEnlaces(siguiente, browser)), ...enlaces];
    }
    await browser.close();
    return enlaces;
}


async function obtenerInformacion(url, currentBrowser) {
    const browser = currentBrowser || await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    await page.goto(url);

    const precio = await page.evaluate(() => {
        const elemento = document.querySelector('meta[itemprop="price"]');
        return elemento ? elemento.content : '';
    });

    const precioAnterior = await page.evaluate(() => {
        const elemento = document.querySelector('.andes-money-amount--previous .andes-money-amount__fraction');
        return elemento ? elemento.textContent : '';
    });

    const esFull = await page.evaluate(() => {
        return !!document.querySelector('.andes-tooltip__trigger .ui-pdp-icon--full');
    });

    const vendidos = await page.evaluate(() => {
        const elemento = document.querySelector(".ui-pdp-subtitle");
        const cadena = elemento ? elemento.textContent : '';
        const match = cadena.match(/\d+/); // Expresión regular para encontrar uno o más dígitos
        return match ? parseInt(match[0]) : 0;
    });

    const imagen = await page.evaluate(() => {
        const elemento = document.querySelector('.ui-pdp-gallery__figure img');
        return elemento ? elemento.src : '';
    });

    const titulo = await page.evaluate(() => {
        const elemento = document.querySelector('.ui-pdp-title');
        return elemento ? elemento.textContent : '';
    });


    await browser.close();

    return [
        titulo, precio, precioAnterior, esFull, vendidos, imagen, url
    ];
}

async function generar(url) {
    const enlaces = await obtenerEnlaces(url);
    const paginas = [['Título', 'Precio', 'Precio anterior', 'Full', 'Vendidos', "Imágen", 'Link']];
    for (const enlace of enlaces) {
        console.log(enlace);
        try {
            // Utilizamos async-retry para realizar reintentos
            const pagina = await retry(async () => {
                // La función makeAsyncRequest será reintentada automáticamente si falla
                return obtenerInformacion(enlace);
            }, {
                retries: 3, // Número máximo de intentos
            });
    
            paginas.push(pagina);
        } catch (error) {
            console.error('Error al obtener los datos:', error);
        }
    }
    const libro = XLSX.utils.book_new();

    // Crear una hoja de cálculo
    const hojaDeCalculo = XLSX.utils.aoa_to_sheet(paginas);

    // Agregar la hoja de cálculo al libro
    XLSX.utils.book_append_sheet(libro, hojaDeCalculo, 'Hoja 1');

    // Escribir el archivo
    XLSX.writeFile(libro, 'archivo.xlsx');
}

generar('https://listado.mercadolibre.com.mx/pagina/detodoonlinecom/listado/');